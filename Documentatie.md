# Problema

Proiectul ales este o aplicatie web "Smart Home Grocery". Cum sugereaza si numele, aplicatia are ca si scop gestionarea cumparaturilor facute de diferite persoane care locuiesc in acelasi apartament. App-ul va fi legat la o baza de date unde se vor stoca printre altele:
  - Datele de autentificare a utilizatorilor
  - Bunurile in momentul de fata in apartament( aceste bunuri se impart in 2 categorii: produse expirabile si nonexpirabile)
  - Detalii despre cumparaturile efectuate

Vor exista 2 tipuri de utilizator: un admin( seful de apartament) si useri( ceilalti). Acesti utilizatori vor fi in stare sa efectueze urmatoarele operatii:
 - admin-ul va fi in stare de a efectua orice operatie pe care un user le poate efectua plus modificarea datelor si stergerea datelor existente, plus depunerea unor cereri de cumparare a produselor folosite in comun
 - user-ii vor putea:
    - depune cereri pentru cumpararea de catre cineva a unor produse pentru propria folosinta
    - interogarea datoriilor proprii
    - accepta o cerere de cumparaturi
 - admin-ul are acces la toate datele, asadar poate interoga datoriile tuturor

In aplicatie se va tine cont de cheltuelile efectuate pentru scop comun in ultimele 1 sau 2 luni.
O data ce un produs expira admin-ul va primi o notificare, si va putea hotara sa stearga sau nu produsul respectiv.

# Solutia

Aplicatia va fi realizata cu ajutorul framework-ului Spring.
Cu ajutorul metodelor puse la dispozitie de catre API, prin endpoint-urile disponibile ale aplicatiei se va obtine un rezultat al interogarii in format JSON.

Aplicatia pune la dispozitie un User Interface pe web, unde se efectueaza operatiile dorite.

Pentru notificarea fiecarui user in parte cand un produs nou este adaugat( original acest lucru ar trebi la adaugarea unui order nou, dar din motive de testare s-a ales ca deocamdata sa ne folosim de adaugarea produselor) s-a ales sa se foloseasca un design pattern si anume Observer Design Pattern.


# Implementarea

## 1. General
In prima faza s-a realizat implementarea operatiilor CRUD( create, retrieve, update, delete) pentru entitatile persistate in baza de date, acestea fiind: User, Product, Order.
Operatiile se implementeaza automat in Bean-uri cu ajutorul framework-ului, prin intermediul interfetei CrudRepository pus la dispozitie de catre Spring,care creaza implementarea metodelor simple si daca se doreste si implementarea unelor metode derivate.

S-au realizat interfete si clase de Service pentru fiecare entitate in parte. Aceste clase de service sunt responsabile pentru a implementa logica fiecarei clase in parte combinata cu persistarea datelor in baza de date cu ajutorul metodelor puse la dispozitie de catre CrudRepository.

Pentru realizarea design pattern-ului Observer entitatea User implementeaza interfata Channel care pune la dispozitie metoda update. In calsa User metoda update implementata va modifica atributul lastNotification. Astfel toti userii vor fi observers. Cand aplicatia e pornita initial se adauga toti userii din baza de date la lista de observers.
Clasa Notifyer implementeaza partea de observable a design pattern-ului. Acesta isi va modifica atributul lui news can un produs este adaugat.

Un Notifyer este creat ca si protected static in clasa UserServiceImpl( aici se realizeaza initializarea listei de observatori cu ajutorul adnotarii @PostConstruct si se adauga fiecare user nou creat), ca acesta sa fie vizibil la nivel de pachet si sa poata fi accesat de catre ProductServiceImpl unde se modifica news-ul la adaugarea unui produs nou.

In controllere se apeleaza metodele puse la dispozitie de catre service-uri.

Cu ajutorul unei aplicatii web locale, consumam serviciile puse la dispozitie de catre cealalta aplicatie.

## 2. Diagrama de pachete

![Picture](Package.png)

## 3. Diagrame de clase

![Picture](User.png)
![Picture](Product.png)
![Picture](Order.png)




