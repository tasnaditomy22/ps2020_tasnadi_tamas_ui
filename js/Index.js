$(document).ready(function()    {
    $("#login").click(function(e)    {
        $.ajax({
            type: 'GET',
            url: 'http://localhost:8080/user/login',
            dataType: 'json',
			data: { "name": $('#name').val(), "password": $('#password').val() },
            success: function(data){
				if( !data.commonOrderRight) var url = "userPage.html";    
				else var url = "adminpage.html";
				
				$(location).attr('href',url);
            },
            error: function(){
                alert('Wrong login credentials'); 
				console.log();
            }

        });
    });
	$("#register").click(function(e)    {
        var url = "register.html";    
		$(location).attr('href',url);
    });


});
