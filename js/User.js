
$(document).ready(function()    {
    $("#update").click(function(e)    {
        $.ajax({
            type: 'PUT',
            url: 'http://localhost:8080/user/update',
            dataType: 'json',
			data: { "name": $('#name').val(), "password": $('#password').val() },
            success: function(data){
                
            },
            error: function(){
                alert('Something went wrong'); 
				console.log();
            }

        });
    });
	
	$("#add").click(function(e)    {
        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/product/add',
            dataType: 'json',
			data: { "name": $('#product_name').val(), "quantity": $('#product_quantity').val(), "price": $('#product_price').val()},
            success: function(data){
                alert('Product added');
            },
            error: function(){
                alert('Something went wrong'); 
				console.log();
            }

        });
    });
	
	
	$("#show").click(function(e)    {
        $.ajax({
            type: 'GET',
            url: 'http://localhost:8080/product/all',
            dataType: 'json',
            success: function(data){
               var html = "<tr> <th>Name</th><th>Quantity</th><th>Price</th></tr>";
				$.each(data,function(key,value){
				  html +='<tr>';
				  html +='<td>'+ value.name + '</td>';
				  html +='<td>'+ value.quantity + '</td>';
				  html +='<td>'+ value.price + '</td>';
				  html +='</tr>';
				});
				$('#myTable').append(html);
            },
            error: function(){
                alert('Something went wrong'); 
				console.log();
            }

        });
    });

});