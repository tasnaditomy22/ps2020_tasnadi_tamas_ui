$(document).ready(function()    {
    $("#delete_b").click(function(e)    {
        $.ajax({
			type: 'DELETE',
            url: 'http://localhost:8080/user/delete',
            dataType: 'json',
			data: {"name": $('#name').val()},
            success: function(data){
                alert('User deleted');
            },
            error: function(){
                alert('Unexpected error'); 
				console.log();
            }

        });
    });
	
	$("#show").click(function(e)    {
        $.ajax({
			type: 'GET',
            url: 'http://localhost:8080/user/all',
            dataType: 'json',
            success: function(data){   
				var html = "<tr> <th>Name</th><th>Password</th><th>Notification</th></tr>";
				$.each(data,function(key,value){
				  html +='<tr>';
				  html +='<td>'+ value.name + '</td>';
				  html +='<td>'+ value.password + '</td>';
				  html +='<td>'+ value.lastNotification + '</td>';
				  html +='</tr>';
				});
				$('#myTable').append(html);
            },
            error: function(){
                alert('Unexpected error'); 
				console.log();
            }

        });
    });


});